const logEnabled = false;
let deck = [];
let nextCardInd = 0;
let visible = [false, false, false, false, false];
// let hiddenCards = 0;
const faces = ['ARTICLE', 'CHAPTER', 'LECTURE'];
const otherFace = 'OTHER';
const colorDict = {
    'ARTICLE': "red",
    'CHAPTER': "black",
    'LECTURE': "red",
    'OTHER': "black"
}
const symbolDict = {
    'ARTICLE': "&#9829;",
    'CHAPTER': "&#9824;",
    'LECTURE': "&#9830;",
    'OTHER': "&#9827;"
}

const defaultFromYear = 2020;
const defaultToYear = 2021;

const totalCards = 5;
let curCards = [null, null, null, null, null];


function drawNewCard(cardIndex, skippedDict){
    while (true){
        let newCard = null;
        if (nextCardInd < deck.length){
            newCard = deck[nextCardInd];
            nextCardInd++;
        }
        else{
            return null;
        }
        let allreadyOnTheTable = false;
        for (let i = 0; i<totalCards; ++i){
            if (i != cardIndex && curCards[i]){
                if (curCards[i]["card_value"] == newCard["card_value"] &&
                    curCards[i]["card_face"] == newCard["card_face"]){
                    console.log("skipping " + nextCardInd);
                    skippedDict["skipped"] += 1;
                    allreadyOnTheTable = true;
                    break;    
                }
            }
        }
        if (allreadyOnTheTable){
            continue;
        }
        curCards[cardIndex] = newCard;
        console.log("Putting " + newCard["card_value"] +"-"+ newCard["card_face"]+" ("+ nextCardInd + ") in posistion " + cardIndex);
        skippedDict["delt"] += 1;
        return newCard;
    }
}

function replaceMissingCards(skippedDict){
    let fail = false;
    for (let i = 0; i<totalCards; ++i){
        if (curCards[i] === null){
            if (!drawNewCard(i, skippedDict)){
                fail = true;
            }
            visualiseCardFaceDown(i);
        }
    }
    return !fail;
}

function resetButtons(){
    document.getElementById("btn-deal-cards").disabled = false;
    document.getElementById("btn-swap-cards").disabled = true;
    document.getElementById("btn-reviel-cards").disabled = true;
    document.getElementById("game-info").innerHTML = "Ready to deal new cards!";
    for (let i = 0; i<totalCards; ++i){
        document.getElementById("card-"+i+"-card").onclick = null;
        document.getElementById("card-"+i+"-swap").checked = false;
    }
}

function revielCards(){
    for (let i = 0; i<totalCards; ++i){
        // visualiseCardFaceUp(i);
        // registering the visibilty
        if (curCards[i]){
            visible[i] = false;
        }
        else{
            visible[i] = true;
        }
        document.getElementById("card-"+i+"-card").onclick = function() { 
            visualiseCardFaceUp(i);
            visible[i] = true;
            for (let i = 0; i<totalCards; ++i){
                if (!visible[i]){
                    return;
                }
            }
            resetButtons();
        };
        document.getElementById("card-"+i+"-swap").checked = false;
    }
    // document.getElementById("btn-deal-cards").disabled = false;
    document.getElementById("game-info").innerHTML = "Click the cards to reviel one-by-one";
    document.getElementById("btn-reviel-cards").disabled = true;
}

function drawNewCards(){
    for (let i = 0; i<totalCards; ++i){
        curCards[i] = null;
    }
    let skippedDict = {
        "skipped" : 0,
        "delt" : 0
    };
    let tmp = replaceMissingCards(skippedDict);
    document.getElementById("cards-info").innerHTML = "Delt " + skippedDict["delt"] + " cards. " 
        + skippedDict["skipped"] + " cards skipped. "
        + (deck.length - nextCardInd)+ " cards left.";
    document.getElementById("game-info").innerHTML = "Select up to five cards to swap." 
    document.getElementById("btn-deal-cards").disabled = true;
    document.getElementById("btn-swap-cards").disabled = false;
    return tmp;
}

function swapCards(){
    let count = 0;
    for (let i = 0; i<totalCards; ++i){
        if (document.getElementById("card-"+i+"-swap").checked){
            // document.getElementById("card-"+i+"-swap").checked = false
            curCards[i] = null;
            count++;
        }
    }
    let skippedDict = {
        "skipped" : 0,
        "delt" : 0
    };
    let tmp = replaceMissingCards(skippedDict);
    document.getElementById("cards-info").innerHTML = 
        "Swapped " + skippedDict["delt"] + " cards. " 
        + skippedDict["skipped"] + " cards skipped. "
        + (deck.length - nextCardInd)+ " cards left.";
    document.getElementById("game-info").innerHTML = "Press \"Reveal cards\" to start revieling.";
    document.getElementById("btn-swap-cards").disabled = true;
    document.getElementById("btn-reviel-cards").disabled = false;
    return tmp;
}

function visualiseCardEmpty(i){
    let faceColor = "black";
    document.getElementById("card-"+i+"-name").innerHTML = "NO-CARD";
    document.getElementById("card-"+i+"-card").classList.remove("with_back");
    document.getElementById("card-"+i+"-card").classList.remove("card");
    //empty text
    document.getElementById("card-"+i+"-face").innerHTML = ".";
    document.getElementById("card-"+i+"-face").style.color = faceColor;

    document.getElementById("card-"+i+"-value").innerHTML = ".";
    document.getElementById("card-"+i+"-value").style.color = faceColor;
    document.getElementById("card-"+i+"-author").innerHTML = "";

}

function visualiseCardFaceDown(cardIndex){
    let i = cardIndex;
    let card = curCards[cardIndex];
    if (card === null){
        visualiseCardEmpty(i);
        //todo call face down visualizations
        return;
    }
    let title = card["card_title"];
    let faceColor = colorDict["OTHER"];
    // changing all things for card i

    console.log("Showing card "+i+" face up.")
    // document.getElementById("card-"+i+"-card").classList.remove("with_back");
    document.getElementById("card-"+i+"-card").classList.add("with_back");
    document.getElementById("card-"+i+"-card").classList.add("card");
    // document.getElementById("card-"+i+"-card").classList.remove("card-side");

    document.getElementById("card-"+i+"-name").innerHTML = title;
    
    document.getElementById("card-"+i+"-face").innerHTML = ".";
    document.getElementById("card-"+i+"-face").style.color = faceColor;

    document.getElementById("card-"+i+"-value").innerHTML = ".";
    document.getElementById("card-"+i+"-value").style.color = faceColor;
    document.getElementById("card-"+i+"-author").innerHTML = "";
}

function visualiseCardFaceUp(cardIndex){
    let i = cardIndex;
    let card = curCards[cardIndex];
    if (card === null){
        visualiseCardEmpty(i);
        //todo call face down visualizations
        return;
    }
    let title = card["card_title"];
    let face = card["card_face"];
    let value = card["card_value"];
    let faceColor = colorDict[face];
    let faceSymbol = symbolDict[face];
    // changing all things for card i

    console.log("Revieling card "+i)
    // document.getElementById("card-"+i+"-card").classList.remove("with_back");
    document.getElementById("card-"+i+"-card").classList.remove("with_back");
    document.getElementById("card-"+i+"-card").classList.add("card");
    // document.getElementById("card-"+i+"-card").classList.remove("card-side");

    document.getElementById("card-"+i+"-name").innerHTML = title;
    
    document.getElementById("card-"+i+"-face").innerHTML = face;
    document.getElementById("card-"+i+"-face").style.color = faceColor;

    if (value == 1){
        value = "A";
    }
    document.getElementById("card-"+i+"-value").innerHTML = value + " " + faceSymbol;
    document.getElementById("card-"+i+"-value").style.color = faceColor;
    document.getElementById("card-"+i+"-author").innerHTML = ". author";
}

function showLog(){
    for (let i = 0; i<deck.length; ++i){
        card = deck[i];
        console.log((i + 1) + ". " + "("+ card["original_index"] + ") " + card["card_value"] + ". author of " + card["card_face"] + " : " + card["card_title"]);
    }
}

// function shuffle from https://stackoverflow.com/a/2450976/4913560
// contributed by coolaj86
// Adopted under CC BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/
// start of copied code
function shuffle(array) {
    let currentIndex = array.length, randomIndex;
  
    // While there remain elements to shuffle...
    while (currentIndex != 0) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }
// end of copied code

function getCardsForResearcher(id, fromYear, untilYear, statusCallBack=null, textPrefix=""){
    console.log(id);
    fetch(`https://api.cristin.no/v2/results?contributor=${id}&published_since=${fromYear}&published_before=${untilYear}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then(function (res) {
        if (!res.ok) {
            alert("Request failed.");
            if (statusCallBack){
                statusCallBack("Loading articles for id " + id + " failed!");
            }
        }
        else {
            res.json().then(function (json) {
                // console.log("got information");
                if (logEnabled) {
                    console.log("data : " + JSON.stringify(json));
                }
                deck = json;
                let totalResults = json.length;
                let text = "Loaded a deck of " + totalResults + " cards."
                if (statusCallBack){
                    statusCallBack(textPrefix);
                }
                document.getElementById("game-info").innerHTML = "New game! Ready to deal cards.";
                document.getElementById("cards-info").innerHTML = text;
                processDeck(id, statusCallBack);
                });
            }
        });
}

function getAuthorNumber(card, researcherId, statusCallBack){
    let address = card["contributors"]["url"];
    console.log(address);
    fetch(address, 
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then(function (res) {
        if (!res.ok) {
            alert("Request failed.");
            if (statusCallBack){
                statusCallBack("Loading authors for id: " + researcherId + " failed!");
            }
        }
        else {
            res.json().then(function (json) {
                // console.log("got information");
                if (logEnabled) {
                    console.log("data : " + JSON.stringify(json));
                }
                let authors = json;
                let authorNum = -1;
                for (let i = 0; i<authors.length; ++i){
                    let author = authors[i];
                    if (author["cristin_person_id"] == researcherId){
                        authorNum = i + 1;
                        break;
                    }
                }
                card["card_value"] = authorNum;
                });
            }
        });
}

function getCardFace(card){
    for (let i = 0; i<faces.length; ++i){
        if (card["category"]["code"].includes(faces[i])){
            card["card_face"] = faces[i];
            return;
        }
    }
    card["card_face"] = otherFace;
}

function getCardName(card){
    card["card_title"] = card["title"][card["original_language"]].replace(/\n/g, " ");
    card["card_title"] = card["card_title"].replace(/\s+/g, " ");
}

function processDeck(researcherId, statusCallBack){
    for (let i = 0; i<deck.length; ++i){
        card = deck[i];
        card["original_index"] = i + 1;
        getAuthorNumber(card, researcherId, statusCallBack);
        getCardFace(card);
        getCardName(card);
    }
    deck = shuffle(deck);
    nextCardInd = 0;
    for (let i = 0; i<totalCards; ++i){
        curCards[i] = null;
        document.getElementById("card-"+i+"-swap").checked = false;
        visualiseCardEmpty(i);
    }
    // document.getElementById("game-info").innerHTML = "Started new game";
    // document.getElementById("btn-deal-cards").disabled = false;
    resetButtons();
}

function getResearcherByName(name, institute, fromYear=null, toYear=null, statusCallBack=null){
    console.log(name);
    fetch("https://api.cristin.no/v2/persons?name="+name+
            "&institution="+institute, 
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then(function (res) {
        if (!res.ok) {
            alert("Request failed.");
            if (statusCallBack){
                statusCallBack("Loading " + name + " failed!");
            }
        }
        else {
            res.json().then(function (json) {
                // console.log("got information");
                if (logEnabled) {
                    console.log("data : " + JSON.stringify(json));
                }
                if (json.length == 1){
                    let researcherInfo = json[0];
                    let total = 10;
                    let id = researcherInfo["cristin_person_id"];
                    let firstName = researcherInfo["first_name"];
                    let lastName = researcherInfo["surname"];
                    let text = "Player: " + firstName + " " + lastName + " (id "+id+").";
                    if (!fromYear){
                        fromYear = defaultFromYear;
                    }
                    if (!toYear){
                        toYear = defaultToYear;
                    }
                    getCardsForResearcher(id, fromYear, toYear, statusCallBack, text);
                }
                else if (json.length == 0){
                    let text = "No researchers found with name " + name;
                    if (statusCallBack){
                        statusCallBack(text);
                    }
                }
                else{
                    let text = "Multiple researchers found with name " + name;
                    if (statusCallBack){
                        statusCallBack(text);
                    }
                }

                });
            }
        });
}

function showDeckInfo(info){
    document.getElementById("deck-info").innerHTML = info;
}

function getDeck(){
    let resName = document.getElementById("researcher-name").value;
    let institute = document.getElementById("institute").value;
    let yearFrom = document.getElementById("year-from").value;
    let yearTo = document.getElementById("year-to").value;
    getResearcherByName(resName, institute, yearFrom, yearTo, showDeckInfo);
    for (let i=0; i<totalCards; ++i){
        curCards[i] = null;
        document.getElementById("card-"+i+"-swap").checked = false;
        visualiseCardFaceUp(i);
    }
}



